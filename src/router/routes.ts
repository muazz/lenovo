import { RouteConfig } from 'vue-router'

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'egitimler', component: () => import('pages/Egitimler.vue')},
      { path: 'urunler', component: () => import('pages/Products.vue') },
      { path: 'urun/:id', component: () => import('pages/Product.vue') },
      { path: 'topluluk', component: () => import('pages/Topluluk.vue') },
      { path: 'soru/:id', component: () => import('pages/ToplulukSoru.vue') },
      { path: 'video/:id', component: () => import('pages/Video.vue') },
      // { path: 'video/2', component: () => import('pages/Video2.vue') },
    ]
  },

  {
    path: '/',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      {path: 'login', component: () => import('pages/Login.vue'), meta: {auth: true}}
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
