import axios from 'axios'
import qs from 'qs'

// const api = "http://lenovo-promotor-test.432designstudio.com"
const api = "http://lenovo.46.101.203.71.nip.io"

const axiosInstance = axios.create({
  paramsSerializer: params => {
    return qs.stringify(params, { arrayFormat: 'repeat' })
  },
  baseURL: api
})

export default ({ Vue }) => {
  Vue.prototype.$axios = axiosInstance
}

export { axiosInstance }
