// export const createNewUser = async function ({commit}, data) {
//   const $fb = this.$fb
//   const {email, password} = data
//   return $fb.createUserWithEmail(email, password)
// }
//
// export const loginUser = async function ({commit}, payload) {
//   const $fb = this.$fb
//   const {email, password} = payload
//   return $fb.loginWithEmail(email, password)
// }
//
// export const logoutUser = async function ({commit}, payload) {
//   const $fb = this.$fb
//   await $fb.logoutUser()
// }
//
// export function routeUserToAuth() {
//   this.$router.push({
//     path: '/auth/login'
//   })
// }


import {axiosInstance} from 'boot/axios'
import {Cookies} from 'quasar'

const REGISTER_ROUTE = '/auth/register'
const VERIFICATION_ROUTE = '/auth/verify'
// const LOGIN_ROUTE = '/oauth/token'
const LOGIN_ROUTE = '/user/login?_format=json'
const FETCH_USER_ROUTE = '/auth/user'
const PASSWORD_FORGOT_ROUTE = '/auth/password/forgot'
const PASSWORD_RESET_ROUTE = '/auth/password/reset'

export function register(state, data) {
  return axiosInstance.post(REGISTER_ROUTE, data)
}

export function login(state, data) {
  return new Promise(function (resolve, reject) {
    // var bodyFormData = new FormData();
    // bodyFormData.append('name', data.name);
    // bodyFormData.append('pass', data.pass);
    // bodyFormData.append('grant_type', data.grant_type);
    // bodyFormData.append('client_id', data.client_id);
    // bodyFormData.append('client_secret', data.client_secret);
    return axiosInstance
    // ({
    //   method: 'post',
    //   url: LOGIN_ROUTE,
    //   data: {name:data.name},
    //   headers: {
    //     'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
    //   }
    // })
      .post(LOGIN_ROUTE, data)
      .then(response => {
        console.log(response)
        state.commit('setUser', response.data.current_user)
        const token = response.data.access_token
        // axiosInstance.defaults.headers.common['X-CSRF-Token'] = token
        axiosInstance.defaults.headers.common['Authorization'] =
          'Bearer ' + token
        state.dispatch('setToken', {
          token: token,
          user: response.data.current_user,
          csrfToken: response.data.csrf_token,
        })
        resolve()
      })
      .catch(error => {
        reject(error)
      })
  })
}

export function setUserState(state , data){
  state.commit('setUser', data)
}

export function setToken(state, data) {
  axiosInstance.defaults.headers.common['X-CSRF-Token'] = data.token
  try{
    Cookies.set('authorization_token', data.token)
    Cookies.set('user', data.user)
    Cookies.set('csrf_token', data.csrfToken)
  }
  catch (e){
    console.log(e)
  }
}

export async function fetch(state) {
  var token = Cookies.get('authorization_token')
  if (token) {
    axiosInstance.defaults.headers.common['X-CSRF-Token'] = token
    return axiosInstance.get(FETCH_USER_ROUTE).then(response => {
      state.commit('setUser', response.data.data)
    })
  }
}

export function logout(state) {
  Cookies.remove('authorization_token')
  state.commit('setUser', null)
}

export function verify(state, token) {
  return axiosInstance.get(VERIFICATION_ROUTE + '?token=' + token)
}

export function passwordForgot(state, data) {
  return axiosInstance.post(PASSWORD_FORGOT_ROUTE, data)
}

export function passwordReset(state, {token, data}) {
  return axiosInstance.post(PASSWORD_RESET_ROUTE + '?token=' + token, data)
}
