export function setAuthState(state, data) {
  state.isAuthenticated = data.isAuthenticated
  state.isReady = data.isReady
}

export function setUser(state, data) {
  if (data) {
    state.user = {
      id: data.uid,
      name: data.name,
      roleNames: data.roles
    }
  } else {
    state.user = null
  }
}
