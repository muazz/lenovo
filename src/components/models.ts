export interface Todo {
  id: number;
  content: string;
  url: string;
  video_url: string;
  video_time: string;
  video_image: string;
}

export interface Meta {
  totalCount: number;
}

export interface VideoIn {
  id: number;
  content: string;
  url: string;
  video_url: string;
  video_time: string;
  video_image: string;
  video_category: string;
  video_credit: string;
  video_add_date: string;
}

export interface VideoSimilar {
  id: number;
  content: string;
  url: string;
  video_url: string;
  video_time: string;
  video_image: string;
  video_category: string;
  video_credit: string;
  video_add_date: string;
}

export interface LastView {
  id: number;
  content: string;
  url: string;
  video_url: string;
  video_time: string;
  video_image: string;
}
